import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Movie } from '../movie.model';

@Component({
  selector: 'media-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
  movies: Movie[];
  constructor(private appComponent: AppComponent) {
    this.movies = appComponent.movies;
   }

  ngOnInit(): void {
  }

}

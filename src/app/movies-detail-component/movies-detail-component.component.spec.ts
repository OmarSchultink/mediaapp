import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesDetailComponentComponent } from './movies-detail-component.component';

describe('MoviesDetailComponentComponent', () => {
  let component: MoviesDetailComponentComponent;
  let fixture: ComponentFixture<MoviesDetailComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesDetailComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

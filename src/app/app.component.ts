import { Component } from '@angular/core';
import { Movie } from './movie.model';
import { MoviesService } from './movies.service';
import { OnInit } from '@angular/core';


@Component({
  selector: 'media-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'MediaApp';
  movies: Movie[];
  moviesService: any;

  constructor(moviesService: MoviesService){
    this.movies = moviesService.getMovies();
  }

  ngOnInit(){
    this.movies = this.moviesService.getMovies();
  }

}







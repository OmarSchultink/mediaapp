import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { BrowserModule } from '@angular/platform-browser';

export const MoviesAppRoutes = [
    { path: 'movies', component: MoviesListComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(MoviesAppRoutes)
    ],
    exports: [RouterModule]
})
export class AppRouterModule {}

